<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\DeviceController;
use App\Http\Controllers\Api\SubscriptionController;
use App\Http\Controllers\GoogleApple\GoogleAppleController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'device', 'name' => 'device.'], function () {
    Route::post('register', [DeviceController::class, 'register'])->name('register');
});
Route::group(['prefix' => 'purchase', 'name' => 'purchase.', 'middleware' => 'api-auth'], function () {
    Route::get('verification', [SubscriptionController::class, 'verify'])->name('verification');
    ;
});

Route::group(['prefix' => 'subscription', 'name' => 'subscription.', 'middleware' => 'api-auth'], function () {
    Route::get('check', [SubscriptionController::class, 'checkSubscription'])->name('check');
});

Route::group(['prefix' => 'google-apple', 'middleware' => 'google-apple-auth'], function () {
    Route::get('device-app-validation', [GoogleAppleController::class, 'deviceAppValidatation'])->name('device.app.validation');
    Route::post('app-purchase', [GoogleAppleController::class, 'purchase'])->name('app.purchase');
    Route::get('receipt-verification', [GoogleAppleController::class, 'receiptVerification'])->name('receipt.verification');
    Route::get('verify-expired-subscription', [GoogleAppleController::class, 'subscriptionVerification'])->name('subscription.verification');
    Route::group(['prefix' => 'subscription'], function(){
        Route::get('cancel', [GoogleAppleController::class, 'cancel'])->name('cancel');
        Route::get('expire', [GoogleAppleController::class, 'expire'])->name('expire');
        Route::get('renew', [GoogleAppleController::class, 'renew'])->name('renew');
    });
});
