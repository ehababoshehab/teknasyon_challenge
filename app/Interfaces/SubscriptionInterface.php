<?php

namespace App\Interfaces;

use App\Http\Requests\SubscriptionVerificationRequest;
use Illuminate\Http\Request;

interface SubscriptionInterface
{
    public function verify(SubscriptionVerificationRequest $request);

    public function checkSubscription(Request $request);
}
