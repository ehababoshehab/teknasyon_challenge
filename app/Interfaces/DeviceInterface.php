<?php

namespace App\Interfaces;

use App\Http\Requests\DeviceRequest;

interface DeviceInterface
{
    public function registerDevice(DeviceRequest $request);
}
