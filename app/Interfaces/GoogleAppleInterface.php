<?php

namespace App\Interfaces;

use App\Http\Requests\GoogleAppleAppPurchase;
use App\Http\Requests\GoogleAppleDeviceAppValidation;
use App\Http\Requests\GoogleAppleReceiptVerification;
use App\Http\Requests\GoogleAppleExpiredSubscriptionVerification;
use App\Http\Requests\GoogleAppleChangeSubsciptionStatus;

interface GoogleAppleInterface
{
    public function deviceAppValidatation(GoogleAppleDeviceAppValidation $request);

    public function purchase(GoogleAppleAppPurchase $request);

    public function receiptVerification(GoogleAppleReceiptVerification $request);

    public function subscriptionVerification($request);

    public function cancel(GoogleAppleChangeSubsciptionStatus $request);

    public function expire(GoogleAppleChangeSubsciptionStatus $request);

    public function renew(GoogleAppleChangeSubsciptionStatus $request);
}
