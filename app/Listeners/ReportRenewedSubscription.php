<?php

namespace App\Listeners;

use App\Events\Renewed;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\ThirdPartyReportSubscription;

class ReportRenewedSubscription
{
    public function handle(Renewed $event)
    {
        ThirdPartyReportSubscription::create([
            'appID' => $event->appID,
            'uID' => $event->uID,
            'event' => $event->status
        ]);
    }
}
