<?php

namespace App\Listeners;

use App\Events\Canceled;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\ThirdPartyReportSubscription;

class ReportCanceledSubscription
{
    public function handle(Canceled $event)
    {
        ThirdPartyReportSubscription::create([
            'appID' => $event->appID,
            'uID' => $event->uID,
            'event' => $event->status
        ]);
    }
}
