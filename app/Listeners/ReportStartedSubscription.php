<?php

namespace App\Listeners;

use App\Events\Started;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\ThirdPartyReportSubscription;

class ReportStartedSubscription
{
    public function handle(Started $event)
    {
        ThirdPartyReportSubscription::create([
            'appID' => $event->appID,
            'uID' => $event->uID,
            'event' => $event->status
        ]);
    }
}
