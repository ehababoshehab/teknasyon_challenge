<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\GoogleAppleAppSubscription;
use App\Models\GooglePlayAppStoreApp;

class GoogleAppleAuth
{

    public function handle(Request $request, Closure $next)
    {
        $username = $request->header('username');
        $password = $request->header('password');

        if ($request->appID) {
            $app = GooglePlayAppStoreApp::where('appID', $request->appID)->where('username', $username)->first();
            if ($app && Hash::check($password, $app->password)) {
                return $next($request);
            }
        }

        if ($request->receipt) {
            $subscription = GoogleAppleAppSubscription::where('receipt', $request->receipt)->first();
            if (!$subscription) {
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid Receipt',
                    'error' => true,
                    'code' => 422,
                ], 422);
            }
            $app = GooglePlayAppStoreApp::find($subscription->google_apple_app_id);
            if ($app && $app->username == $username && Hash::check($password, $app->password)) {
                return $next($request);
            }
        }

        return response()->json([
            'success' => false,
            'message' => 'Unauthorized',
            'error' => true,
            'code' => 401,
        ], 401);
    }
}
