<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Repositories\DeviceRepository;

class ApiAuth
{

    public function __construct(DeviceRepository $deviceRepository)
    {
        $this->deviceRepository = $deviceRepository;
    }

    public function handle(Request $request, Closure $next)
    {
        $token = $request->header('Authorization');
        $device = $this->deviceRepository->getDeviceByToken($token);

        if ($token && $device) {
            return $next($request);
        }

        return response()->json([
            'message' => 'Unauthorized',
            'error' => true,
            'code' => 401,
        ], 401);
    }
}
