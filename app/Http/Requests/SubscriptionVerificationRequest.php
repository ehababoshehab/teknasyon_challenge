<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class SubscriptionVerificationRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "receipt" => "required",
            "token" => "required|exists:tokens",
            "appID" => "required|exists:applications"
        ];
    }

    public function messages()
    {
        return [
            'token.exists' => 'Invalid token',
            'appID.exists' => 'Invalid appID',
            'appID.required' => 'appID field is required'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json([
                'message' => $validator->errors(),
                'error' => true,
                'code' => 422,
            ], 422)
        );
    }
}
