<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class GoogleAppleDeviceAppValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "appID" => "required|exists:google_play_app_store_apps",
            "os" => "required|exists:google_play_app_store_apps",
            "uID" => "required|exists:google_apple_devices"
        ];
    }

    public function messages()
    {
        return [
            'appID.exists' => 'Invalid appID',
            'appID.required' => 'appID field is required',
            'os.exists' => 'Invalid os',
            'uID.exists' => 'Invalid uID',
            'uID.required' => 'uID field is required',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json([
                'success' => false,
                'message' => $validator->errors(),
                'error' => true,
                'code' => 422,
            ], 422)
        );
    }
}
