<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class GoogleAppleChangeSubsciptionStatus extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "receipt" => "required|exists:google_apple_app_subscriptions"
        ];
    }

    public function messages()
    {
        return [
            'receipt.exists' => 'Invalid receipt'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json([
                'success' => false,
                'message' => $validator->errors()->messages(),
                'error' => true,
                'code' => 422,
            ], 422)
        );
    }
}
