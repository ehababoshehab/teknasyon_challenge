<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class GoogleAppleExpiredSubscriptionVerification extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "receipt" => "required|exists:google_apple_app_subscriptions",
            "uID" => "required|exists:google_apple_app_subscriptions",
            "appID" => "required|exists:google_play_app_store_apps",
            "os" => "required|exists:google_play_app_store_apps",
        ];
    }

    public function messages()
    {
        return [
            'receipt.exists' => 'Invalid receipt',
            'appID.exists' => 'Invalid appID',
            'appID.required' => 'appID field is required',
            'uID.exists' => 'Invalid uID',
            'uID.required' => 'uID field is required',
            'os.exists' => 'Invalid os'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json([
                'success' => false,
                'message' => $validator->errors()->messages(),
                'error' => true,
                'code' => 422,
            ], 422)
        );
    }
}
