<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Traits\ResponseAPI;

class DeviceRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "uID" => "required|exists:google_apple_devices",
            "appID" => "required|exists:applications",
            "language" => "required",
            "os" => "required|in:GOOGLE,APPLE"
        ];
    }

    public function messages()
    {
        return [
            'appID.exists' => 'Invalid appID',
            'appID.required' => 'appID field is required',
            'uID.exists' => 'Invalid uID',
            'uID.required' => 'uID field is required',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json([
                'message' => $validator->errors(),
                'error' => true,
                'code' => 422,
            ], 422)
        );
    }
}
