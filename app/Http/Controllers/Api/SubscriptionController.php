<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Interfaces\SubscriptionInterface;
use App\Http\Requests\SubscriptionVerificationRequest;

class SubscriptionController extends Controller
{
    protected $subscriptionInterface;

    public function __construct(SubscriptionInterface $subscriptionInterface)
    {
        $this->subscriptionInterface = $subscriptionInterface;
    }

    public function verify(SubscriptionVerificationRequest $request)
    {
        return $this->subscriptionInterface->verify($request);
    }

    public function checkSubscription(Request $request)
    {
        return $this->subscriptionInterface->checkSubscription($request);
    }
}
