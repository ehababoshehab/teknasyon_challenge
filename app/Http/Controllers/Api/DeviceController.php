<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\DeviceRequest;
use App\Interfaces\DeviceInterface;

class DeviceController extends Controller
{

    protected $deviceInterface;

    public function __construct(DeviceInterface $deviceInterface)
    {
        $this->deviceInterface = $deviceInterface;
    }

    public function register(DeviceRequest $request)
    {
        return $this->deviceInterface->registerDevice($request);
    }
}
