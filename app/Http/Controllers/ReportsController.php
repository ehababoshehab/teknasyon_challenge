<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Subscription;

class ReportsController extends Controller
{
    public function report(Request $request)
    {
        $where = [];
        $subscriptions = Subscription::with('device', 'application');
        if ($request->status && $request->expired != "yes") {
            $subscriptions->where('status', $request->status);
        }
        if ($request->verified == "yes") {
            $subscriptions->whereNotNull('verified_at');
        }
        if ($request->expired == "yes") {
            $subscriptions->where('status', 'Expired')->where('expire_at', '<', \Carbon\Carbon::now());
        }
        if ($request->verified_from) {
            $subscriptions->where('verified_at', '>=', $request->verified_from);
        }
        if ($request->verified_to) {
            $subscriptions->where('verified_at', '<=', $request->verified_to);
        }
        if ($request->expire_from) {
            $subscriptions->where('expire_at', '>=', $request->expire_from);
        }
        if ($request->expire_to) {
            $subscriptions->where('expire_at', '<=', $request->expire_to);
        }
        $subscriptions = $subscriptions->get();
        return view('report')->with('subscriptions', $subscriptions);
    }
}
