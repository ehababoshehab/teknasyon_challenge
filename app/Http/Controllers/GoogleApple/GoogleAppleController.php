<?php

namespace App\Http\Controllers\GoogleApple;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\GoogleAppleInterface;
use App\Http\Requests\GoogleAppleAppPurchase;
use App\Http\Requests\GoogleAppleDeviceAppValidation;
use App\Http\Requests\GoogleAppleReceiptVerification;
use App\Http\Requests\GoogleAppleExpiredSubscriptionVerification;
use App\Http\Requests\GoogleAppleChangeSubsciptionStatus;

class GoogleAppleController extends Controller
{
    protected $googleAppleInterface;

    public function __construct(GoogleAppleInterface $googleAppleInterface)
    {
        $this->googleAppleInterface = $googleAppleInterface;
    }

    public function deviceAppValidatation(GoogleAppleDeviceAppValidation $request)
    {
        return $this->googleAppleInterface->deviceAppValidatation($request);
    }

    public function purchase(GoogleAppleAppPurchase $request)
    {
        return $this->googleAppleInterface->purchase($request);
    }

    public function receiptVerification(GoogleAppleReceiptVerification $request)
    {
        return $this->googleAppleInterface->receiptVerification($request);
    }

    public function subscriptionVerification(GoogleAppleExpiredSubscriptionVerification $request)
    {
        return $this->googleAppleInterface->subscriptionVerification($request);
    }

    public function cancel(GoogleAppleChangeSubsciptionStatus $request)
    {
        return $this->googleAppleInterface->cancel($request);
    }

    public function expire(GoogleAppleChangeSubsciptionStatus $request)
    {
        return $this->googleAppleInterface->expire($request);
    }

    public function renew(GoogleAppleChangeSubsciptionStatus $request)
    {
        return $this->googleAppleInterface->renew($request);
    }
}
