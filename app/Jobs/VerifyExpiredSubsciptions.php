<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Subscription;
use App\Repositories\SubscriptionRepository;

class VerifyExpiredSubsciptions implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    private $subscriptionRepository;

    private $subscription;

    public function __construct(Subscription $subscription)
    {
        $this->subscription = $subscription;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(SubscriptionRepository $subscriptionRepository)
    {
        $this->subscriptionRepository = $subscriptionRepository;
        $subscription = $this->subscription;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => route('subscription.verification') .
            '?uID=' . $subscription->device->uID .
            '&appID=' . $subscription->application->appID .
            '&receipt=' . $subscription->receipt .
            '&store=' . $subscription->device->os,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            ));
        $response = curl_exec($curl);
        $response = json_decode($response, true);
        curl_close($curl);

        $this->subscriptionRepository->updateSubscriptionStatus($response, $subscription);
    }
}
