<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Subscription;
use App\Jobs\VerifyExpiredSubsciptions as VerifyExpiredSubsciptionsJob;
use Carbon\Carbon;

class VerifyExpiredSubsciptions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'verifyExpiredSubsciptions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $subscriptions = Subscription::with('device')->where('status', '!=', 'Canceled')->where('expire_at', '<=', Carbon::now())->get();
        if ($subscriptions->count() > 0) {
            foreach ($subscriptions as $subscription) {
                    VerifyExpiredSubsciptionsJob::dispatch($subscription);
            }
        }
    }
}
