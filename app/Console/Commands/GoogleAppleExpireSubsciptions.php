<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Models\GoogleAppleAppSubscription;

class GoogleAppleExpireSubsciptions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'google-apple-expire-subscriptions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $subscriptions = GoogleAppleAppSubscription::where('status', '!=', 'Canceled')->where('expire_at', '<=', Carbon::now())->get();
        if ($subscriptions->count() > 0) {
            foreach ($subscriptions as $subscription) {
                    $subscription->update(['status' => 'Expired']);
            }
        }
    }
}
