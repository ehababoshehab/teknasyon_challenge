<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class Canceled
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    public $appID;
    public $status;
    public $uID;

    public function __construct($appID, $uID, $status)
    {
        $this->appID = $appID;
        $this->uID = $uID;
        $this->status = $status;
    }
}
