<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ThirdPartyReportSubscription extends Model
{
    use HasFactory;

    protected $fillable = ['uID', 'appID', 'event'];
}
