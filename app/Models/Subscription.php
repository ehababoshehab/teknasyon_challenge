<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['device_id', 'application_id', 'status', 'receipt', 'verified_at', 'expire_at'];

    public function device()
    {
        return $this->belongsTo(Device::class);
    }

    public function application()
    {
        return $this->belongsTo(Application::class);
    }
}
