<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['uID', 'language', 'os'];


    public function token()
    {
        return $this->hasOne(Token::class, 'device_id');
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class, 'device_id');
    }
}
