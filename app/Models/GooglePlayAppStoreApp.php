<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\GoogleAppleAppSubscription;

class GooglePlayAppStoreApp extends Model
{
    use HasFactory;

    protected $fillable = ['appID', 'os', 'username', 'password'];

    public function subscription()
    {
        return $this->hasMany(GoogleAppleAppSubscription::class);
    }
}
