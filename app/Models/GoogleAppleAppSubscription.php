<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\GooglePlayAppStoreApp;

class GoogleAppleAppSubscription extends Model
{
    use HasFactory;

    protected $fillable = ['google_apple_app_id', 'uID', 'receipt', 'status', 'expire_at'];

    public function app()
    {
        return $this->hasMany(GooglePlayAppStoreApp::class);
    }
}
