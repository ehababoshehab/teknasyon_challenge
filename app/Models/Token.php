<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;

class Token extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['device_id', 'token'];

    public function device()
    {
        return $this->belongsTo(Device::class);
    }
}
