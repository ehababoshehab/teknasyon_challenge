<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    use HasFactory;

    protected $fillable = ['appID', 'os', 'username', 'password'];

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class, 'device_id');
    }
}
