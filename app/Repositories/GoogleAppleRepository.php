<?php

namespace App\Repositories;

use Carbon\Carbon;
use App\Interfaces\GoogleAppleInterface;
use App\Http\Requests\GoogleAppleAppPurchase;
use App\Http\Requests\GoogleAppleDeviceAppValidation;
use App\Http\Requests\GoogleAppleReceiptVerification;
use App\Http\Requests\GoogleAppleExpiredSubscriptionVerification;
use App\Http\Requests\GoogleAppleChangeSubsciptionStatus;
use App\Models\GoogleAppleDevices;
use App\Models\GooglePlayAppStoreApp;
use App\Models\GoogleAppleAppSubscription;

class GoogleAppleRepository implements GoogleAppleInterface
{
    public function deviceAppValidatation(GoogleAppleDeviceAppValidation $request)
    {
        $app = GooglePlayAppStoreApp::where('appID', $request->appID)->where('os', $request->os)->first();
        $device = GoogleAppleDevices::where('uID', $request->uID)->where('os', $request->os)->first();
        if ($app && $device) {
            return response()->json([
                'success' => true,
                'code' => 200,
                'message' => 'OK',
                'result' => [
                    'app' => $app,
                    'device' => $device
                ]
            ]);
        }
        return response()->json([
            'success' => false,
            'code' => 422,
            'message' => 'Invalid Device or App or OS'
        ]);
    }

    public function purchase(GoogleAppleAppPurchase $request)
    {
        try {
            $app = GooglePlayAppStoreApp::where('appID', $request->appID)->first();

            $checkSubsciption = GoogleAppleAppSubscription::where('google_apple_app_id', $app->id)
            ->where('uID', $request->uID)
            ->where('expire_at', '>=', Carbon::now())
            ->where(function ($query) {
                $query->where('status', 'Started')
                      ->orWhere('status', 'Renewed');
            })->first();

            if ($checkSubsciption) {
                return response()->json([
                    'success' => true,
                    'code' => 200,
                    'message' => 'The subsciption already exists',
                    'receipt' => $checkSubsciption->receipt
                ]);
            }

            if ($app->os == 'GOOGLE') {
                $expireAt = Carbon::now()->addDays(10);
            } else {
                $expireAt = Carbon::now(-6)->addDays(10);
            }

            $receipt = md5(uniqid($request->uID . $request->appID, true)) . mt_rand(1000, 9999);
            $status = 'Started';
            if (substr($receipt, -1) % 2 == 0) {
                $status = 'Failed';
            }

            GoogleAppleAppSubscription::create([
                'google_apple_app_id' => $app->id,
                'uID' => $request->uID,
                'receipt' => $receipt,
                'status' => $status,
                'expire_at' => $expireAt,
            ]);

            return response()->json([
                'success' => true,
                'code' => 201,
                'message' => 'OK',
                'receipt' => $receipt
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'code' => $e->getCode(),
                'message' => $e->getMessage()
            ]);
        }
    }

    public function receiptVerification(GoogleAppleReceiptVerification $request)
    {
        $purchase = GoogleAppleAppSubscription::where('receipt', $request->receipt)->first();
        $app = GooglePlayAppStoreApp::find($purchase->google_apple_app_id)->appID;
        $lastChar = substr($purchase->receipt, -1);
        if ($lastChar % 2 == 0) {
            return response()->json([
                'success' => false,
                'message' => "Invalid Receipt (Because last character in the receipt is an even number) (" . $purchase->receipt . "), You will need to call the purchase API again",
                'error' => true,
                'code' => 422,
            ], 422);
        }

        $response['app'] = $app;
        $response['uID'] = $purchase->uID;
        $response['status'] = $purchase->status;
        $response['expire_at'] = $purchase->expire_at;
        $response['receipt'] = $purchase->receipt;
        $response['success'] = true;
        return response()->json($response, 200);
    }

    public function subscriptionVerification($request)
    {
        try {
            $app = GooglePlayAppStoreApp::where('appID', $request->appID)->where('os', $request->os)->first();

            $subscription = GoogleAppleAppSubscription::where('receipt', $request->receipt)->where('google_apple_app_id', $app->id)->where('uID', $request->uID)->first();

            $lastChar = substr($subscription->receipt, -1);

            if ($lastChar % 6 == 0) {
                return response()->json([
                    'success' => false,
                    'message' => "Rate limit exceeded",
                    'error' => true,
                    'code' => 422,
                ], 422);
            }

            $response['status'] = $subscription->status;
            $response['uID'] = $subscription->uID;
            $response['receipt'] = $subscription->receipt;
            $response['appID'] = $app->appID;
            $response['expire_at'] = $subscription->expire_at;
            $response['os'] = $app->os;


            return response()->json([
                'success' => true,
                'code' => 200,
                'message' => 'OK',
                'result' => $response
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'code' => $e->getCode(),
                'message' => $e->getMessage()
            ]);
        }
    }

    public function cancel(GoogleAppleChangeSubsciptionStatus $request)
    {
        $subscription = GoogleAppleAppSubscription::where('receipt', $request->receipt)->update(['status' => 'Canceled']);
        return response()->json([
            'success' => true,
            'code' => 201,
            'message' => 'Subscription Canceled!',
            'result' => $request->receipt
        ]);
    }

    public function expire(GoogleAppleChangeSubsciptionStatus $request)
    {
        $subscription = GoogleAppleAppSubscription::where('receipt', $request->receipt)->update([
            'status' => 'Expired',
            'expire_at' => Carbon::now()->addDays(-1)
        ]);
        return response()->json([
            'success' => true,
            'code' => 201,
            'message' => 'Subscription Expired!',
            'result' => $request->receipt
        ]);
    }

    public function renew(GoogleAppleChangeSubsciptionStatus $request)
    {
        $subscription = GoogleAppleAppSubscription::where('receipt', $request->receipt)->update([
            'status' => 'Renewed',
            'expire_at' => Carbon::now()->addDays(10)
        ]);
        return response()->json([
            'success' => true,
            'code' => 201,
            'message' => 'Subscription Renewd!',
            'result' => $request->receipt
        ]);
    }
}
