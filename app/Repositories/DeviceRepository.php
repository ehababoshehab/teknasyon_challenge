<?php

namespace App\Repositories;

use App\Http\Requests\DeviceRequest;
use App\Interfaces\DeviceInterface;
use App\Traits\ResponseAPI;
use App\Models\Application;
use App\Models\Token;
use App\Models\Device;
use App\Models\Subscription;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class DeviceRepository implements DeviceInterface
{
    // Use ResponseAPI Trait in this repository
    use ResponseAPI;

    protected $token;
    protected $device;
    protected $subscription;
    protected $application;

    public function __construct(Device $device, Subscription $subscription, Token $token, Application $application)
    {
        $this->token = $token;
        $this->device = $device;
        $this->subscription = $subscription;
        $this->application = $application;
    }

    public function registerDevice(DeviceRequest $request)
    {
        try {
            // Validate APP and DEVICE (IF exist in Google and Apple platform)
            $validateDeviceApp = $this->deviceAppValidation($request->uID, $request->appID, $request->os);
            if (!$validateDeviceApp->success) {
                return $this->error(
                    $validateDeviceApp->message,
                    422
                );
            }

            // Get the device information from the request
            $deviceInfo['uID'] = $request->uID;
            $deviceInfo['language'] = $request->language;
            $deviceInfo['os'] = $request->os;

            // Register the device in the databse if not exist
            $device = $this->device::firstOrCreate($deviceInfo);

            $application = $this->application::where('appID', $request->appID)->first();

            // Register the app in the databse if not exist
            $subscription = $this->subscription::firstOrCreate(['device_id' => $device->id, 'application_id' => $application->id]);

            // If the device does not have client token generate new one
            $response['client-token'] = $device->token ? $device->token->token : $this->token::create(['device_id' => $device->id, 'token' => md5(uniqid($device->uID, true))])->token;

            return $this->success(
                $device->wasRecentlyCreated ? "Device registered" : "Device already exist",
                $response,
                $device->wasRecentlyCreated ? 201 : 200
            );
        } catch (QueryException $e) {
            return $this->error($e->getMessage(), $e->getCode());
        } catch (ModelNotFoundException $e) {
            return $this->error($e->getMessage(), $e->getCode());
        } catch (\Exception $e) {
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    public function getDeviceByToken($token)
    {
        $token = $this->token->where('token', $token)->first();
        if ($token) {
            return $token->device;
        }
        return $token;
    }

    public function deviceAppValidation($uID, $appID, $os)
    {
        $app = Application::where('appID', $appID)->first();
        $request = Request::create(route('device.app.validation'), 'GET', [
            'appID' => $appID,
            'os' => $os,
            'uID' => $uID
        ]);
        $request->headers->set('Content-Type', 'application/json');
        $request->headers->set('username', $app->username);
        $request->headers->set('password', $app->password);
        $res = Route::dispatch($request);
        $res = json_decode($res->getContent());
        return $res;
    }
}
