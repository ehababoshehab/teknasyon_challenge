<?php

namespace App\Repositories;

use Carbon\Carbon;

use App\Traits\ResponseAPI;
use App\Repositories\DeviceRepository;
use App\Interfaces\SubscriptionInterface;
use App\Http\Requests\SubscriptionVerificationRequest;

use App\Models\Application;
use App\Models\Subscription;

use App\Events\Started;
use App\Events\Renewed;
use App\Events\Canceled;
use App\Jobs\VerifyExpiredSubsciptions;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;

use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SubscriptionRepository implements SubscriptionInterface
{
    // Use ResponseAPI Trait in this repository
    use ResponseAPI;

    protected $subscription;
    protected $deviceRepository;
    protected $application;

    public function __construct(DeviceRepository $deviceRepository, Subscription $subscription, Application $application)
    {
        $this->deviceRepository = $deviceRepository;
        $this->subscription = $subscription;
        $this->application = $application;
    }

    public function verify(SubscriptionVerificationRequest $request)
    {
        try {
            $receipt = $request->receipt;
            $token = $request->token;
            $device = $this->deviceRepository->getDeviceByToken($token);
            if ($subscription = $this->subscription::where('device_id', $device->id)->where('receipt', $receipt)->whereNotNull('verified_at')->first()) {
                return $this->success(
                    'Verfied',
                    $subscription,
                    200
                );
            }

            // Validate APP and DEVICE (IF exist in Google and Apple platform)
            $verifyReceipt = $this->receiptVerification($receipt, $request->appID);
            if (!$verifyReceipt->success) {
                return $this->error(
                    $verifyReceipt->message,
                    422
                );
            }

            if ($verifyReceipt->uID != $device->uID) {
                return $this->error(
                    'Verification Faild (Wrong Device)',
                    422
                );
            }

            if ($device->os == 'APPLE') {
                $expireAt = Carbon::parse($verifyReceipt->expire_at)->addHours(6);
            } else {
                $expireAt = $verifyReceipt->expire_at;
            }

            $application = $this->application::where('appID', $request->appID)->first();

            $subscription = $this->subscription::firstOrCreate([
                'device_id' => $device->id,
                'application_id' => $application->id,
            ]);

            $subscription->update([
                'status' => $verifyReceipt->status,
                'verified_at' => Carbon::now(),
                'expire_at' => $expireAt,
                'receipt' => $receipt,
            ]);

            if ($verifyReceipt->status == "Started") {
                event(new Started($verifyReceipt->app, $device->uID, $verifyReceipt->status));
            } elseif ($verifyReceipt->status == "Renewed") {
                event(new Renewed($verifyReceipt->app, $device->uID, $verifyReceipt->status));
            } elseif ($verifyReceipt->status == "Canceled") {
                event(new Canceled($verifyReceipt->app, $device->uID, $verifyReceipt->status));
            }

            return $this->success(
                'Verfied',
                $subscription,
                201
            );

            return $this->error(
                $verifyReceipt->message,
                422
            );
        } catch (QueryException $e) {
            return $this->error($e->getMessage(), $e->getCode());
        } catch (ModelNotFoundException $e) {
            return $this->error($e->getMessage(), $e->getCode());
        } catch (\Exception $e) {
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    public function checkSubscription(Request $request)
    {
        $device = $this->deviceRepository->getDeviceByToken($request->header('Authorization'));
        $subscriptions = $device->subscriptions->whereNotNull('application_id');

        $data = [];
        foreach ($subscriptions as $subscription) {
            $data[$subscription->application->appID] = $subscription->status;
        }
        return $this->success(
            'Subscriptions',
            $data,
            200
        );
    }

    public function updateSubscriptionStatus($response, $subscription)
    {
        try {
            if ($response['success'] == true) {
                if ($response['result']['os'] == 'APPLE') {
                    $expireAt = Carbon::parse($response['result']['expire_at'])->addHours(6);
                } else {
                    $expireAt = $response['result']['expire_at'];
                }

                $subscription->update([
                    'status' => $response['result']['status'],
                    'expire_at' => $expireAt
                ]);

                if ($response['result']['status'] == "Started") {
                    event(new Started($response['result']['appID'], $response['result']['uID'], $response['result']['status']));
                } elseif ($response['result']['status'] == "Renewed") {
                    event(new Renewed($response['result']['appID'], $response['result']['uID'], $response['result']['status']));
                } elseif ($response['result']['status'] == "Canceled") {
                    event(new Canceled($response['result']['appID'], $response['result']['uID'], $response['result']['status']));
                }
            } elseif ($response['success'] == false && $response['message'] == 'Rate limit exceeded') {
                VerifyExpiredSubsciptions::dispatch($subscription);
            }

            return true;
        } catch (QueryException $e) {
            return $this->error($e->getMessage(), $e->getCode());
        } catch (ModelNotFoundException $e) {
            return $this->error($e->getMessage(), $e->getCode());
        } catch (\Exception $e) {
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    public function receiptVerification($receipt, $appID)
    {
        $app = Application::where('appID', $appID)->first();
        $request = Request::create(route('receipt.verification'), 'GET', [
            'receipt' => $receipt
        ]);
        $request->headers->set('Content-Type', 'application/json');
        $request->headers->set('username', $app->username);
        $request->headers->set('password', $app->password);
        $res = Route::dispatch($request);
        $res = json_decode($res->getContent());
        return $res;
    }
}
