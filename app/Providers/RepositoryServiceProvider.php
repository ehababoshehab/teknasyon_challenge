<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        // Register Interface and Repository in here
        // You must place Interface in first place
        // If you dont, the Repository will not get readed.
        $this->app->bind(
            'App\Interfaces\DeviceInterface',
            'App\Repositories\DeviceRepository'
        );

        $this->app->bind(
            'App\Interfaces\SubscriptionInterface',
            'App\Repositories\SubscriptionRepository'
        );

        $this->app->bind(
            'App\Interfaces\GoogleAppleInterface',
            'App\Repositories\GoogleAppleRepository'
        );
    }
}
