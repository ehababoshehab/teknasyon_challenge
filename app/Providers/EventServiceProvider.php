<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use App\Events\Started;
use App\Events\Renewed;
use App\Events\Canceled;
use App\Listeners\ReportStartedSubscription;
use App\Listeners\ReportRenewedSubscription;
use App\Listeners\ReportCanceledSubscription;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Started::class => [
            ReportStartedSubscription::class,
        ],

        Renewed::class => [
            ReportRenewedSubscription::class,
        ],

        Canceled::class => [
            ReportCanceledSubscription::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
