# Live #

Server URL (Reporting Interface) : ```http://161.35.62.89/```

Server URL (Reporting Interface) : ```http://161.35.62.89/report```

API URL : ```http://161.35.62.89/api/```


You can find postman API export in ```app/postman/Teknasyon.postman_collection.json```

Note : I have some permission issues on my MacBookPro, There was no way for the docker to work. But I had it running on linux. On the live server I am using supervisor to manage the queue. But since docker did not work on my Mac I changed the way but did not try test it on MAC. I made on Linux.

Note : You can not register any device or subscribe any app not exists in the database. Please check the seeder file inorder to know what is the devices and applications that I am generating. ```/database/seedersGooglePlayAppStoreAppsSeeder.php```

### Enjoy! ###

================================================================================


# Installation : #

### 1- Clone repo : ###
```
git clone https://ehababoshehab@bitbucket.org/ehababoshehab/teknasyon_challenge.git
```

### 2- Go inside the root directory : ###
```
docker-compose -f docker-compose.yml up -d --build
```

### 3- Enter the app container : ###
```
docker-compose -f docker-compose.yml run laravel-app bash
```

### 4- go to apache root directory and install packages ###
```
cd /var/www/html && composer install
```


### 5- create .env file by copying the .env.exmple ###
```
cp .env.example .env
```

### 6- Generate key ###
```
php artisan key:generate
```

### 7- Give permission to storage directory ###
```
chmod -R 777 storage
```
NOTE : here i am giving it 777 not 775, because the files under root permission, so apache user will not be able to write in storage with 775

### 8- Migration ###
```
php artisan migrate --seed
```
I have created a seed file that generate around 10 applications and 10 devices, you can not work on random values for apps and devices with the apis

Note : If you are using docker make sure each of 80,3306 and 8080 ports are not in use by another service in your environment or you will need to change from ```docker-compose.yml``` file
Note : If you do not want to use docker, you can use homestead web server by running ```php artisan serve```

================================================================================


# APIs #
You can find the API examples exported to from postman inside postman directory :

### 1- Device Registration ###
* Api : `api/device/register`

* Type : ```POST```

* Body Ex.```{
    "uID" : "GOOGLEPHONE_1",
    "appID" : "GOOGLEAPP_1",
    "language" : "English",
    "os" : "GOOGLE"
}```

* Success Response Ex. :```{
    "message": "Device registered",
    "error": false,
    "code": 201,
    "results": {
        "client-token": "b244f238862cc229ed9840be5f37700b"
    }
}```

* Success Response Ex. : ```{
    "message": "Device already exist",
    "error": false,
    "code": 200,
    "results": {
        "client-token": "b244f238862cc229ed9840be5f37700b"
    }
}```

* Error Response : ```{
    "message": {
        "uID": [
            "uID field is required"
        ],
        "appID": [
            "appID field is required"
        ],
        "language": [
            "The language field is required."
        ],
        "os": [
            "The os field is required."
        ]
    },
    "error": true,
    "code": 422
}```

* Error Response : ```
{
    "message": {
        "uID": [
            "Invalid uID"
        ]
    },
    "error": true,
    "code": 422
}```


### 2- Payment Request From Mobile Client to Google/Apple platform (App purchase) ###
* Api : ```api/google-apple/app-purchase```

* Type : ```POST```

* Body Ex. : ```{
    "uID" : "GOOGLEPHONE_1",
    "appID" : "GOOGLEAPP_1"
}```

* Header Ex. : ```
username:google.app.1
password:password```

* Success Response Ex. : ```{
    "success": true,
    "code": 201,
    "message": "OK",
    "receipt": "6c2606b8451a1a278b6836e079ccc1ee9475"
}```

* Success Response Ex. : ```{
    "success": true,
    "code": 200,
    "message": "The subsciption already exists",
    "receipt": "6c2606b8451a1a278b6836e079ccc1ee9475"
}```

* Error Response : ```{
    "success": false,
    "message": "Unauthorized",
    "error": true,
    "code": 401
}```

* Error Response : ```{
    "message": "Invalid Receipt (Because last character in the receipt is an even number) (f0790067beec6caab1bd57c43cf1ef4a1030), You will need to call the purchase API again",
    "error": true,
    "code": 422
}```

* Error Response : ```
{
    "message": {
        "uID": [
            "Invalid uID"
        ]
    },
    "error": true,
    "code": 422
}```

### 3- Receipt Verification ###
* Api : ```api/purchase/verification?&receipt=&token=&appID=```

* Type : ```GET```

* Params Ex. : ```{
    receipt : 471930903de407c49e2578fb1a563d08831,
	token : 879fa9538c288dce8188aad68ad08cc,
	appID : GOOLEAPP_1,
}```

* Header Ex. : ```Authorization : b244f238862cc229ed9840be5f37700b```

* Success Response Ex. : ```{
    "message": "Verfied",
    "error": false,
    "code": 201,
    "results": {
        "id": 1,
        "device_id": 1,
        "appID": "GOOGLEAPP_1",
        "status": "Started",
        "receipt": "6c2606b8451a1a278b6836e079ccc1ee9475",
        "verified_at": "2021-06-01T15:35:35.091587Z",
        "expire_at": "2021-06-11 15:25:07",
        "deleted_at": null,
        "created_at": "2021-06-01T15:16:50.000000Z",
        "updated_at": "2021-06-01T15:35:35.000000Z"
    }
}```

* Error Response : ```{
    "success": false,
    "message": "Unauthorized",
    "error": true,
    "code": 401
}```

* Error Response : ```{
    "message": {
        "token": [
            "Invalid token"
        ],
        "appID": [
            "Invalid appID"
        ]
    },
    "error": true,
    "code": 422
}```


### 4- Check Subsciption Status ###
* Api : ```api/subscription/check```

* Type : ```GET```

* Header Ex. : ```Authorization : b244f238862cc229ed9840be5f37700b```

* Success Response Ex. : ```{
    "message": "Subscriptions",
    "error": false,
    "code": 200,
    "results": {
        "GOOGLEAPP_1": "Started",
    }
}```

* Success Response Ex. : ```{
    "message": "Subscriptions",
    "error": false,
    "code": 200,
    "results": {
        "GOOGLEAPP_1": "Started",
		"GOOGLEAPP_2": "Started",
    }
}```

* Error Response : ```{
    "success": false,
    "message": "Unauthorized",
    "error": true,
    "code": 401
}```

### 5- Device and App Validation / Google/Apple platform ###
* Api : ```api/google-apple/device-app-validation?os=&appID=&uID=```

* Type : ```GET```

* Params Ex. : ```{
    os : GOOGLE,
	appID : GOOLEAPP_1,
	uID : GOOGLEPHONE_1,
}```

* Header Ex. : ```
username:google.app.1
password:password```

* Success Response Ex. : ```{
    "success": true,
    "code": 200,
    "message": "OK",
    "result": {
        "app": {
            "id": 1,
            "appID": "GOOGLEAPP_1",
            "os": "GOOGLE",
            "username": "google.app.1",
            "password": "$2y$10$0UNxX8bdnuuFtu2lpBty..D86BAsb6YmzmqWA8YjKloNqwF3euIV.",
            "created_at": "2021-06-01T14:45:54.000000Z",
            "updated_at": "2021-06-01T14:45:54.000000Z"
        },
        "device": {
            "id": 2,
            "uID": "GOOGLEPHONE_1",
            "os": "GOOGLE",
            "created_at": "2021-06-01T14:48:14.000000Z",
            "updated_at": "2021-06-01T14:48:14.000000Z"
        }
    }
}```

* Error Response : ```{
    "success": false,
    "message": "Unauthorized",
    "error": true,
    "code": 401
}```

* Error Response : ```{
    "success": false,
    "message": {
        "os": [
            "Invalid os"
        ]
    },
    "error": true,
    "code": 422
}```


### 6- Renew - Cancel - Expire APIs / Google/Apple platform ###

I have created those 3 APIs in order we need to take a fast action while testing, for example the moment the subscription started will expire in 10 days (hardcoded not dynamic). So we can Expire or cancel using this API, and the worker will check the the changed status and update back in the main API platform

* Api Renew : ```/api/google-apple/subscription/renew?receipt=```

* Api Renew : ```/api/google-apple/subscription/cancel?receipt=```

* Api Renew : ```/api/google-apple/subscription/expire?receipt=```

* Type : ```GET```

* Params Ex. : ```{
    receipt : 8c83f3aad7a0e26a8e395d65d177d5a98721
}```

* Header Ex. : ```
username:google.app.1
password:password```

* Success Response Ex. : ```{
    "success": true,
    "code": 201,
    "message": "Subscription Expired!",
    "result": "8c83f3aad7a0e26a8e395d65d177d5a98721"
}```

* Success Response Ex. : ```{
    "success": true,
    "code": 201,
    "message": "Subscription Canceled!",
    "result": "8c83f3aad7a0e26a8e395d65d177d5a98721"
}```

* Success Response Ex. : ```{
    "success": true,
    "code": 201,
    "message": "Subscription Renewed!",
    "result": "8c83f3aad7a0e26a8e395d65d177d5a98721"
}```


================================================================================

# Worker #

* I have created a consol command that fetching all expired subsciptions by ```expire_at``` and status still no updated to ```Expired```
* and pass each result to queued job that is checking the status on Google-Apple platform using this API : ```GET : /api/google-apple/verify-expired-subscription?receipt=&uID=&appID=&os=```
* The command is running with ```Cronjob``` every minute
* Jobs are being executed using ```Cronjob``` also


================================================================================



# CAllback #

* The Events have been created and running, but I have go an issue with the internal api call (Call an API from-to the same project (API consuming )).



================================================================================



# REPORTING #

* I have created an interface for generating a report ```/``` or ```/report```


================================================================================



# Error trapping #

* I have used FormRequest and Custom Validation for each request.
* I have used ```try{...} catch() {...}```



================================================================================



# PSR2 compatibility #

* I have used ```squizlabs/php_codesniffer```
* You can write a code in php and then run this command ```vendor/bin/phpcbf --standard=PSR2``` It will detect and auto fix, Or we can create our own standards and apply it.



================================================================================



# object-oriented programming principles #

* I have used Repositories and Interfaces design pattern.


================================================================================
