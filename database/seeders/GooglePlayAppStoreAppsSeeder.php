<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\Application;
use App\Models\GoogleAppleDevices;
use App\Models\GooglePlayAppStoreApp;

class GooglePlayAppStoreAppsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 500; $i++) {
            $app = GooglePlayAppStoreApp::create([
                'appID' => 'GOOGLEAPP_' . $i,
                'os' => 'GOOGLE',
                'username' => 'google.app.' . $i,
                'password' => Hash::make('password')
            ]);
            Application::create([
                'appID' => 'GOOGLEAPP_' . $i,
                'os' => 'GOOGLE',
                'username' => 'google.app.' . $i,
                'password' => 'password'
            ]);
            echo "App : $app->appID created \n";
            $app = GooglePlayAppStoreApp::create([
                'appID' => 'APPLEAPP_' . $i,
                'os' => 'APPLE',
                'username' => 'apple.app.' . $i,
                'password' => Hash::make('password')
            ]);
            Application::create([
                'appID' => 'APPLEAPP_' . $i,
                'os' => 'APPLE',
                'username' => 'apple.app.' . $i,
                'password' => 'password'
            ]);
            echo "App : $app->appID created \n";
        }
        for ($j = 1; $j < 500; $j++) {
            $device = GoogleAppleDevices::create([
                'uID' => 'APPLEPHONE_' . $j,
                'os' => 'APPLE'
            ]);
            echo "Device : $device->uID created \n";
            $device = GoogleAppleDevices::create([
                'uID' => 'GOOGLEPHONE_' . $j,
                'os' => 'GOOGLE'
            ]);
            echo "Device : $device->uID created \n";
        }
    }
}
