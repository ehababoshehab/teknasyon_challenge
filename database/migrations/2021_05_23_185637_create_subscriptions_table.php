<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('device_id');
            $table->unsignedBigInteger('application_id');
            $table->enum('status', ['Started', 'Renewed', 'Expired', 'Pending', 'Canceled'])->nullable();
            $table->string('receipt')->nullable();
            $table->datetime('verified_at')->nullable();
            $table->datetime('expire_at')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('application_id')->references('id')->on('applications');
            $table->foreign('device_id')->references('id')->on('devices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
