<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGoogleAppleAppSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('google_apple_app_subscriptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('google_apple_app_id');
            $table->string('uID');
            $table->string('receipt');
            $table->enum('status', ['Started', 'Renewed', 'Canceled', 'Failed', 'Expired']);
            $table->datetime('expire_at');
            $table->timestamps();


            $table->foreign('google_apple_app_id')->references('id')->on('google_play_app_store_apps');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('google_apple_app_subscriptions');
    }
}
