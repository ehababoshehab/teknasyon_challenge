<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    </head>
    <body class="antialiased">
        <div class="container">
            <div class="">
                <form class="" action="/report" method="get">
                    <div class="row">
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group">
                                <label for="">Status</label>
                                <select class="form-control" name="status">
                                    <option value="">-Choose</option>
                                    <option value="Started">Started</option>
                                    <option value="Renewed">Renewed</option>
                                    <option value="Canceled">Canceled</option>
                                    <option value="Expired">Expired</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group">
                                <label for="">Verified</label>
                                <select class="form-control" name="verified">
                                    <option value="">-Choose</option>
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group">
                                <label for="">Expired</label>
                                <select class="form-control" name="expired">
                                    <option value="">-Choose</option>
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group">
                                <label for="">Verify From</label>
                                <input type="date" name="verified_from" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group">
                                <label for="">Verify To</label>
                                <input type="date" name="verified_to" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group">
                                <label for="">Expire From</label>
                                <input type="date" name="expire_from" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group">
                                <label for="">Expire To</label>
                                <input type="date" name="expire_to" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="form-group">
                                <label for=""></label>
                                <input type="submit" name="" value="Generate" class="btn btn-lg btn-info">
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-3 col-sm-12">
                    Subscriptions Count : <strong>{{$subscriptions->count()}}</strong>
                </div>
                <div class="col-md-3 col-sm-12">
                    Devices Count : <strong>{{$subscriptions->groupby('device_id')->count()}}</strong>
                </div>
                <div class="col-md-3 col-sm-12">
                    Apps Count : <strong>{{$subscriptions->groupby('application_id')->count()}}</strong>
                </div>
            </div>
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">appID</th>
                  <th scope="col">uID</th>
                  <th scope="col">Status</th>
                  <th scope="col">Verified</th>
                  <th scope="col">Expire</th>
                </tr>
              </thead>
              <tbody>
                  @foreach ($subscriptions as $subscription)
                      <tr>
                        <th scope="row">{{$subscription->id}}</th>
                        <td>{{$subscription->application->appID}}</td>
                        <td>{{$subscription->device->uID}}</td>
                        <td>
                            <span
                                @if ($subscription->status == 'Started' || $subscription->status == 'Renewed')
                                    class="text-success"
                                @elseif ($subscription->status == 'Canceled' || $subscription->status == 'Expired')
                                    class="text-danger"
                                @else
                                    class="text-warning"
                                @endif
                            >{{$subscription->status}}
                            </span>
                        </td>
                        <td>{{$subscription->verified_at}}</td>
                        <td>{{$subscription->expire_at}}</td>
                      </tr>
                  @endforeach
              </tbody>
            </table>
        </div>


        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </body>
</html>
